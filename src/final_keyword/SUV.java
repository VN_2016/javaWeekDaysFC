package final_keyword;

public class SUV extends Car{

	
	
	// inheritance is --- IS_A relationship
	
	//SUV IS-A Car 
	
	int speed;
	
	public void start(){
		System.out.println("IN SUV - start function");
	}
	
	public void move(){
		System.out.println("In SUV- Move function");
	}
	
	
}
