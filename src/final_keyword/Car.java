package final_keyword;

public final class Car {
	
	// properties - variables
	String model;
	String mfr;
	final int wheels=6;
	double price;
	String color;
	
	
	
	// behaviours - functions 
	
	public final void start(){
		System.out.println("In CAR start function");
		// generate a random number
	}
	
	public void stop(){
		System.out.println("In CAR stop function");

	}
	
	
}
