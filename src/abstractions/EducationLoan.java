package abstractions;

public class EducationLoan extends Loan {

	@Override
	public void documentations() {
		// TODO Auto-generated method stub
		System.out.println("submitted documentations for educational loan");
	}

	@Override
	public void calculateInterest() {
		// TODO Auto-generated method stub
		System.out.println("calculated interest for educational loan");

	}

	@Override
	public void calculateEmi() {
		// TODO Auto-generated method stub
		System.out.println("calculated emi for educational loan");

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		System.out.println("closing educational loan");

	}

}
