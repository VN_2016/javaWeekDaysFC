package abstractions;

public abstract class Loan {

	String accNo;
	double amount;
	
	public  void apply(){
		System.out.println("Applying for loan");
	}

	public abstract void documentations();
	
	public void disburse(){
		System.out.println("disburse loan amount : " + amount);		
	}
	
	public abstract void calculateInterest();
	public abstract void calculateEmi();
	
	public void repay(){
		System.out.println("repaying the loan");
	}
	
	public abstract void close();

	
	
}
