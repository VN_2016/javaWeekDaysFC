package inheritance;

public class Car {
	
	// properties - variables
	String model;
	String mfr;
	int wheels;
	double price;
	String color;
	
	
	
	// behaviours - functions 
	
	public void start(){
		System.out.println("In CAR start function");
		// generate a random number
	}
	
	public void stop(){
		System.out.println("In CAR stop function");

	}
	
	
}
