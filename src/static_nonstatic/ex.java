package static_nonstatic;

public class ex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Car c= new Car();
		Car c1= new Car();
		Car c2= new Car();
		
		c.wheels=8;
		System.out.println("***********************");

		System.out.println("c.wheels : "+ c.wheels);
		System.out.println("c1.wheels : "+ c1.wheels);
		System.out.println("c2.wheels : "+ c2.wheels);
		
		System.out.println("***********************");

	
		c1.wheels=5;
		
		System.out.println("c.wheels : "+ c.wheels);
		System.out.println("c1.wheels : "+ c1.wheels);
		System.out.println("c2.wheels : "+ c2.wheels);
		
		System.out.println("***********************");

		c2.wheels=4;
		
		
		System.out.println("c.wheels : "+ c.wheels);
		System.out.println("c1.wheels : "+ c1.wheels);
		System.out.println("c2.wheels : "+ c2.wheels);
		
		System.out.println("***********************");

		c1.wheels=12;
		System.out.println("c.wheels : "+ c.wheels);
		System.out.println("c1.wheels : "+ c1.wheels);
		System.out.println("c2.wheels : "+ c2.wheels);
		
	}

}
