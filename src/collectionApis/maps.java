package collectionApis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class maps {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		// key-value data structure
		
		
		HashMap<String, Object> map= new HashMap<String,Object>();
		map.put("Deepak", "1111111");
		map.put("Santosh", "22222222");
		map.put("reema", "333333333");
		map.put("amit", "444444444");
		
		
		
		System.out.println(map.size());
		
		System.out.println(map.get("Santosh"));
		
		
		System.out.println(map.get("xyz"));
		
		map.put("amit", "555555555555");
		
		map.put("Amit", "9999999999999");
		
		
		System.out.println(map);
		
		map.put("amit", null);
		
		System.out.println(map);
//		map.put(null, null);
		
		System.out.println(map);
		
		
		Set set=map.keySet();
		
		
		Iterator it = set.iterator();
		while(it.hasNext()){
			String key= it.next().toString();
			System.out.println(key + " : "+ map.get(key));
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
