package polymorphism;

public class calc {
	
	// same class
	//same function name
	// diff types/no of parameters
	
	// overloading - compiletime polymorphism

	public void add(int i, int j){
		System.out.println("The sum of i and j : "+ (i+j));
	}
	
	public void add(int i, int j, int k){
		System.out.println("The sum of i and j and k : "+ (i+j+k));
	}
	
	public void add(int i, int j, int k, int l){
		System.out.println("The sum of i and j and k and l : "+ (i+j+k+l));
	}
	
	public void add(int i, int j, int k, String l){
		System.out.println("The sum of i and j and k and l : "+ (i+j+k+l));
	}
	
	
}
