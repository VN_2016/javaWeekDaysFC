package exceptionHandling;

public class throw_keyword {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int age =5;
		
		if(age<18){
			try {
				throw new Exception("Blocker ");
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}
		
		
	}

}
