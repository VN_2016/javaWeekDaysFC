package exceptionHandling;

public class sample {

	
	public static void main(String[] args) {

		try{
		String str=null;
		str.split("[ ]");
		
		int j=3/0;
		
		System.out.println(j);
		}catch(NullPointerException e){
			System.out.println("Nullpointer occured : "+ e.getMessage());
		}catch(ArithmeticException e){
			System.out.println("Exception occeured : " + e.getMessage());
		}catch(Throwable t){
			System.out.println("Error occured : " + t.getMessage());
		}finally{
			System.out.println("In finally");
		}
		
		System.out.println("Line after exception");
		
	}

}
